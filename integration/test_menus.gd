extends Node

var paused = null
var panel = null
var press_keys = []

onready var controller = $controller

func _on_Timer_timeout():
	test()

func test():
	get_tree().paused = paused

	for key in press_keys:
		press_key(key)

	# Save the current paused state
	paused = get_tree().paused

	get_tree().paused = false

# Fake pressing and releasing a key
func press_key(action):
	# Press the key
	var input = InputEventAction.new()
	input.action = action
	input.pressed = true
	Input.parse_input_event(input)

	# Releasing the key
	input.pressed = false
	Input.parse_input_event(input)
