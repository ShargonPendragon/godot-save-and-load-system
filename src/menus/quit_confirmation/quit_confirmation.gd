# This sets up the buttons for a menu panel
extends PanelContainer

signal update_menu_controller(func_name, func_prop)

onready var setup_screen = load("res://src/menus/components/setup_screen.gd").new(self)

func _ready():
	add_child(setup_screen)

# Public: Called when need to switch to another panel
#
# func_prop = Name of the panel to switch to.
#
# Example
#	switch_panel("pause")
func switch_panel(func_prop:String) -> void:
	emit_signal("update_menu_controller", "switch_panel", func_prop)
