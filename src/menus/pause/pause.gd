# This sets up the buttons for a menu panel
extends PanelContainer

signal update_menu_controller(func_name, func_prop)

onready var setup_screen = load("res://src/menus/components/setup_screen.gd").new(self)

func _ready():
	add_child(setup_screen)

# Public: Called when need to switch to another panel
#
# func_prop = Name of the panel to switch to.
#
# Example
#	switch_panel("pause")
func switch_panel(func_prop:String) -> void:
	emit_signal("update_menu_controller", "switch_panel", func_prop)

	if func_prop == "main":
		change_scene_to("res://src/menus/controller.tscn")

# Internal: Changes current sceen to provided scene path
#
# path = Is the path to the new scene
#
# Example
#	change_scene_to("res://src/menus/controller.tscn")
func change_scene_to(path:String) -> void:
	if get_tree().change_scene(path) != OK:
		push_error("Unable to load scene " + path)
