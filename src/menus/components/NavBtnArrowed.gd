extends ToolButton

export(Texture) var icon_hover

var icon_normal

func _ready():
	icon_normal = get_button_icon()

func _on_NavBtn_focus_entered():
	set_button_icon(icon_hover)

func _on_NavBtn_focus_exited():
	set_button_icon(icon_normal)

func _on_mouse_entered():
	grab_focus()

func _on_mouse_exited():
	set_button_icon(icon_normal)
