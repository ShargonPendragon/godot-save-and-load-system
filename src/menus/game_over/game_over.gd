# This sets up the buttons for a menu panel
extends PanelContainer

signal update_menu_controller(func_name)

var functions = []

onready var setup_screen = load("res://src/menus/components/setup_screen.gd").new(self)

func _ready():
	add_child(setup_screen)
	functions = [funcref(self, "reset_game")]

# Public: Reset the game to the current scene
func reset_game() -> void:
	if get_tree().reload_current_scene() != OK:
		push_error("Unable to reset game")

	emit_signal("update_menu_controller", "continue_game", "")
