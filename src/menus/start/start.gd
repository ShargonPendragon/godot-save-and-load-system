extends PanelContainer

signal update_menu_controller(func_name)

func _unhandled_input(_event: InputEvent) -> void:
	if visible:
		emit_signal("update_menu_controller", "switch_panel", "main")
