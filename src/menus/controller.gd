extends Panel

var screen_panels:Dictionary = {}
var current_panel

func _ready() -> void:

	get_panels(self)

	if self.get_parent().get_class() == "Viewport":
		pause_game()
		switch_panel("start")

# Internal: This gets all the panels and puts them in a dictionary
#
# node = Contains the node that will be searched for PanelContainers.
#
# Example
#	get_panels(controller.tscn)
func get_panels(node:Node) -> void:
	for child in node.get_children():
		if child is PanelContainer:
			screen_panels[child.get_name()] = child

		if child.get_child_count() > 0:
			get_panels(child)

# Internal: Called every time there is a change in the inputs
#
# event - A InputEvent that contain information about the change that acured.
#
# Example
#	_unhandled_input(InputEvent)
func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_cancel") and current_panel != screen_panels["main"]:
		if current_panel == screen_panels["pause"]:
			continue_game()
		else:
			pause_screen()

# Public: Pauses the game and makes the pause screen visible
func pause_game() -> void:
	get_tree().paused = true
	set_visible(true)

# Internal: Called to set up the Pause Screen when the Esc Key is pressed
func pause_screen() -> void:
	pause_game()
	switch_panel("pause")

# Public: Unpauses the game and makes the pause screen invisible
func continue_game() -> void:
	get_tree().paused = false
	set_visible(false)
	switch_panel("")

# Public: Called when needing to changed to a different menu screen
#
# new_panel = The name of the panel that we are switching to.
#
# Example
#	switch_panel("pause")
func switch_panel(screen_name:String) -> void:
	var new_panel = screen_panels.get(screen_name)
	if current_panel:
		current_panel.set_visible(false)

	current_panel = new_panel
	if new_panel:
		current_panel.set_visible(true)

# Public: Pause the game and displays the game over screen
func game_over() -> void:
	pause_game()
	switch_panel("game_over")

# Public: Quits that game when called
func quit_game() -> void:
	get_tree().quit()

# Public: Runs the provided fuction provided by the screen
#
# func_name = Name of the function needing called.
# func_prop = Contains the property the function being called needs.
#
# Example:
#	update_menu_controller("switch_panel", "pause")
func update_menu_controller(func_name:String, func_prop:String) -> void:
	if self.has_method(func_name):
		if func_prop:
			self.call(func_name, func_prop)
		else:
			self.call(func_name)
