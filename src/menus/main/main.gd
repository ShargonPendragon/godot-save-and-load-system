# This sets up the buttons for a menu panel
extends PanelContainer

signal update_menu_controller(func_name, func_prop)

onready var setup_screen = load("res://src/menus/components/setup_screen.gd").new(self)

func _ready():
	add_child(setup_screen)

# Public: Changes the scene to the start of the game
func start_game() -> void:
	if get_tree().change_scene("res://src/levels/Game.tscn") != OK:
		push_error("Unable to start game")

	emit_signal("update_menu_controller", "continue_game", "")
