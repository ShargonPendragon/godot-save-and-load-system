extends "res://addons/gut/test.gd"

var subject = null

func before_each():
    subject = preload('res://src/menus/controller.tscn').instance()
    add_child_autofree(subject)

func test_when_get_panels_is_called():
    subject.screen_panels = {}

    subject.get_panels(subject)

    for child in subject.get_node("CenterContainer").get_children():
        assert_eq(subject.screen_panels.has(child.get_name()),true, child.get_name() + "was added to screen panels varable")

func test_when_pause_game_is_called():
    get_tree().paused = false
    subject.set_visible(true)

    subject.pause_game()

    assert_eq(get_tree().paused, true,"Game is paused")
    assert_eq(subject.is_visible(), true, "Pause Controller is visible")

    get_tree().paused = false

func test_when_pause_screen_is_called():
    subject.current_panel = null

    subject.pause_screen()

    assert_eq(get_tree().paused, true,"Game Paused was called")
    assert_eq(subject.is_visible(), true, "Game Paused was called")
    assert_eq(subject.current_panel, subject.screen_panels["pause"], "Current Panel was changed to the pause menu panel")

    get_tree().paused = false

func test_when_continue_game_is_called():
    get_tree().paused = true
    subject.visible = true
    subject.current_panel = subject.screen_panels["pause"]

    subject.continue_game()

    assert_eq(get_tree().paused, false, "Game is unpaused")
    assert_eq(subject.is_visible(), false, "Pause Controller is invisible")
    assert_eq(subject.current_panel, null, "Pause Panel has been switched to Null")

class TestSwitchPanel:
    extends "res://addons/gut/test.gd"

    var class_subject = null
    var test_panel = null

    func before_each():
        class_subject = preload('res://src/menus/controller.tscn').instance()
        add_child_autofree(class_subject)
        test_panel = PanelContainer.new()
        autofree(test_panel)

    func test_when_switch_panel_is_called_when_current_pannel_is_not_null():
        test_panel.visible = true
        class_subject.current_panel = test_panel
        class_subject.screen_panels["pause"].visible = false
        class_subject.switch_panel("pause")

        assert_eq(test_panel.is_visible(), false, "Previous Panel is made invisiable")
        assert_eq(class_subject.current_panel, class_subject.screen_panels["pause"], "Current Panel is Updated to New Panel")
        assert_eq(class_subject.screen_panels["pause"].is_visible(), true, "New Panel is set to visiable")

    func test_when_switch_panel_is_called_null():
        class_subject.current_panel = class_subject.screen_panels["pause"]

        class_subject.switch_panel("")

        assert_eq(class_subject.current_panel, null, "Current Panel has been changed to null")

func test_when_game_over_is_called():
    get_tree().paused = false
    subject.visible = false

    subject.game_over()

    assert_eq(get_tree().paused, true,"Game Paused was called")
    assert_eq(subject.is_visible(), true, "Game Paused was called")
    assert_eq(subject.current_panel, subject.screen_panels["game_over"], "Current Panel is set to the Game Over Panel")

    get_tree().paused = false

func test_when_update_menu_controller_is_called():
    subject.current_panel = null

    subject.update_menu_controller("switch_panel", "pause")

    assert_eq(subject.current_panel.get_name(), "pause")
    