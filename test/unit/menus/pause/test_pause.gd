extends "res://addons/gut/test.gd"

var subject = null

func before_each():
    subject = partial_double("res://src/menus/pause/pause.tscn").instance()
    add_child_autofree(subject)

func test_when_switch_panel_is_called_with_pause():
    watch_signals(subject)
    
    subject.switch_panel("pause")

    assert_signal_emitted_with_parameters(subject, "update_menu_controller", ["switch_panel", "pause"])
    assert_not_called(subject, "change_scene_to")


func test_when_switch_panel_is_called_with_main():
    watch_signals(subject)
    stub(subject, "change_scene_to").to_do_nothing()
    
    subject.switch_panel("main")

    assert_signal_emitted_with_parameters(subject, "update_menu_controller", ["switch_panel", "main"])
    assert_called(subject, "change_scene_to", ["res://src/menus/controller.tscn"])
    