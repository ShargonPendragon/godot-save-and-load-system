extends "res://addons/gut/test.gd"

var subject = null
var screen = null

func before_each():
    screen = load("res://src/menus/game_over/game_over.tscn").instance()
    add_child_autofree(screen)
    subject = screen.setup_screen

func test_when_setup_is_called():
    var test_screen = load("res://src/menus/pause/pause.tscn").instance()
    add_child_autofree(test_screen)
    subject.first_to_focus = null

    subject.setup(test_screen)

    assert_ne(subject.first_to_focus, null, "first_to_focus variable was updated")
    assert_connected(subject.first_to_focus, subject, "pressed")

func test_when_on_visibility_change_is_called_when_visiable_and_first_to_focus():
    screen.visible = true

    subject._on_visibility_changed()

    assert_eq(screen.get_focus_owner(), subject.first_to_focus, "Grab Focus worked")

func test_when_on_visibility_change_is_called_when_not_visiable():
    screen.visible = false

    subject._on_visibility_changed()

    assert_eq(screen.get_focus_owner(), null, "Grab Focus was not called")

class TestButtonPressed:
    extends "res://addons/gut/test.gd"

    var subject = null
    var screen = null
    
    func before_each():
        screen = load("res://src/menus/game_over/game_over.tscn").instance()
        add_child_autofree(screen)
        subject = screen.setup_screen

    func test_when_button_pressed_is_called_with_valid_method_and_no_property():
        watch_signals(screen)

        subject.button_pressed("reset_game")

        assert_signal_emitted_with_parameters(screen, "update_menu_controller", ["continue_game", ""])
        assert_signal_emit_count(screen, "update_menu_controller", 1)

    func test_when_button_pressed_is_called_with_a_non_valid_method():
        watch_signals(screen)

        subject.button_pressed("TEST")

        assert_signal_emitted_with_parameters(screen, "update_menu_controller", ["TEST", null])
        assert_signal_emit_count(screen, "update_menu_controller", 1)
