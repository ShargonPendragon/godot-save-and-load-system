extends "res://addons/gut/test.gd"

var subject = null
var screen = null

func before_each():
    subject = load("res://src/menus/quit_confirmation/quit_confirmation.tscn").instance()
    add_child_autofree(subject)

func test_when_switch_panel_is_called():
    watch_signals(subject)
    
    subject.switch_panel("pause")

    assert_signal_emitted_with_parameters(subject, "update_menu_controller", ["switch_panel", "pause"])
    
