extends "res://addons/gut/test.gd"

var subject = null

func before_each():
    subject = partial_double("res://src/menus/save_system/load/load.gd").new()
    subject.slots = autofree(VBoxContainer.new())
    subject.save_slot_handler = autofree(preload("res://src/menus/save_system/components/save_slot_handler.gd").new(subject, subject.slots))
    subject.file_handler = double("res://src/menus/save_system/components/file_handler.gd").new()

class TestOnDeletePressed:
    extends "res://addons/gut/test.gd"

    var subject = null

    func before_each():
        subject = partial_double("res://src/menus/save_system/load/load.gd").new()
        subject.slots = autofree(VBoxContainer.new())
        subject.save_slot_handler = autofree(preload("res://src/menus/save_system/components/save_slot_handler.gd").new(subject, subject.slots))
        subject.file_handler = double("res://src/menus/save_system/components/file_handler.gd").new()

        for _number in range(3):
            subject.slots.add_child(autofree(Button.new()))

    func test_when_on_delete_pressed_is_called_current_slot_zero():

        stub(subject, "update_load_delete_buttons")

        subject.save_slot_handler.cur_slot = 0

        subject._on_delete_pressed()
        assert_called(subject.file_handler, "remove_save_file_by_index")
        assert_eq(subject.slots.get_child_count(), 2, "Remove Current Save Slot was called")
        assert_called(subject, "update_load_delete_buttons")

    func test_when_on_delete_pressed_is_called_current_slot_not_zero():

        stub(subject, "update_load_delete_buttons")

        subject.save_slot_handler.cur_slot = 1

        subject._on_delete_pressed()
        assert_called(subject.file_handler, "remove_save_file_by_index")
        assert_eq(subject.slots.get_child_count(), 2, "Remove Current Save Slot was called")
        assert_not_called(subject, "update_load_delete_buttons")

func test_when_on_save_slot_pressed_is_pressed():
    var test_button = autofree(Button.new())
    subject.save_slot_handler.cur_slot = 15

    stub(subject, "update_load_delete_buttons")

    subject._on_save_slot_pressed(test_button)

    assert_eq(subject.save_slot_handler.cur_slot, test_button.get_index(), "Current Slot variable was updated")
    assert_called(subject, "update_load_delete_buttons", [false])

func test_when_switch_panel_is_called():
    watch_signals(subject)
    
    subject.switch_panel("pause")

    assert_signal_emitted_with_parameters(subject, "update_menu_controller", ["switch_panel", "pause"])

class TestOnVisibilityChanged:
    extends "res://addons/gut/test.gd"

    var subject = null
    
    func before_each():
        subject = partial_double("res://src/menus/save_system/load/load.gd").new()
    
    func test_when_on_visibility_changed_is_called_visible_true():
        subject.visible = true
        stub(subject, "refresh_screen").to_do_nothing()

        subject._on_visibility_changed()

        assert_called(subject, "refresh_screen")

    func test_when_on_visibility_changed_is_called_visible_false():
        subject.visible = false

        subject._on_visibility_changed()

        assert_not_called(subject, "refresh_screen")

func test_when_update_load_delete_buttons_is_called():
    subject.setup_screen = autofree(load("res://src/menus/components/setup_screen.gd").new(subject))
    subject.setup_screen.buttons = {"delete" : autofree(Button.new()), "load" : autofree(Button.new())}
    for key in subject.setup_screen.buttons:
        subject.setup_screen.buttons[key].disabled = false

    subject.update_load_delete_buttons(true)

    for key in subject.setup_screen.buttons:
        assert_eq(subject.setup_screen.buttons[key].disabled, true)

func test_when_refresh_screen_is_called():
    for _number in range(3):
        subject.slots.add_child(autofree(Button.new()))

    stub(subject, "update_load_delete_buttons").to_do_nothing()

    subject.refresh_screen()

    assert_called(subject.file_handler, "refresh_save_files_list")
    assert_eq(subject.slots.get_child_count(), 1, "Refesh save slots was called")
    assert_called(subject, "update_load_delete_buttons", [true])
