extends "res://addons/gut/test.gd"

var subject = null

func before_each():
    subject = partial_double("res://src/menus/save_system/save/popup.gd").new()
    autofree(subject)

    subject.line_edit = autofree(LineEdit.new())

class TestCancel:
    extends "res://addons/gut/test.gd"

    var subject = null
    
    func before_each():
        subject = partial_double("res://src/menus/save_system/save/popup.gd").new()
        autofree(subject)

    func test_when_cancel_is_called_when_visible():
        subject.visible = true
        watch_signals(subject)

        subject.cancel()

        assert_eq(subject.visible, false, "visible was updated to false")

    func test_when_cancel_is_called_when_not_visible():
        subject.visible = false
        watch_signals(subject)

        subject.cancel()

        assert_eq(subject.visible, false, "visible remained false")
    
func test_when_set_default_text_is_called():
    subject.set_default_text("TEST")

    assert_eq(subject.line_edit.text, "TEST", "The text box test was updated")

func test_when_accept_is_called():
    watch_signals(subject)
    subject.line_edit.text = "TEST"

    subject.accept()

    assert_signal_emitted_with_parameters(subject, "file_name", ["TEST"])
     