extends "res://addons/gut/test.gd"

var subject = null

func before_each():
    subject = partial_double("res://src/menus/save_system/save/save.tscn").instance()
    add_child_autofree(subject)

    subject.file_handler = double("res://src/menus/save_system/components/file_handler.gd").new()

    subject.popup = double("res://src/menus/save_system/save/popup.tscn").instance()
    add_child_autofree(subject.popup)

func test_when_on_new_slot_pressed_is_called():
    subject.setup_screen.buttons["delete"].disabled = false
    subject.save_slot_handler.cur_slot = 5

    subject._on_new_slot_pressed()

    assert_called(subject, "enable_save_button")
    assert_eq(subject.save_slot_handler.cur_slot, 0, "Current Slot variable updated")
    assert_eq(subject.setup_screen.buttons["delete"].disabled, true)

func test_when_on_save_slot_pressed_is_pressed():
    var test_button = autofree(Button.new())
    subject.save_slot_handler.cur_slot = 15
    subject.setup_screen.buttons["delete"].disabled = true

    subject._on_save_slot_pressed(test_button)

    assert_called(subject, "enable_save_button")
    assert_eq(subject.save_slot_handler.cur_slot, test_button.get_index(), "Current Slot variable was updated")
    assert_eq(test_button.disabled, false, "Button disabled was updated to false")

func test_when_enable_save_button_is_called_when_disable_true():
    subject.setup_screen.buttons["save"].disabled = true

    subject.enable_save_button()

    assert_eq(subject.setup_screen.buttons["save"].disabled, false, "Save button Disabled variable was updated to false")

func test_when_enable_save_button_is_called_when_disable_false():
    subject.setup_screen.buttons["save"].disabled = false

    subject.enable_save_button()

    assert_eq(subject.setup_screen.buttons["save"].disabled, false, "Save button Disabled variable was not updated")

func test_when_update_save_slot_is_called():
    stub(subject, "update_data").to_do_nothing()
    subject.save_slot_handler.cur_slot = 0

    subject.update_save_slot()

    assert_called(subject, "update_data")
    assert_called(subject.file_handler, "remove_save_file_by_index")
    assert_called(subject.file_handler, "open_save_file")
    assert_eq(subject.save_slot_handler.cur_slot, 1, "Move Current Save Slot To Top function called")

func test_when_on_save_pressed_is_called_cur_slot_zero():
    stub(subject, "create_save_slot").to_do_nothing()
    stub(subject, "update_save_slot").to_do_nothing()
    stub(subject.file_handler, "create_save_file_name").to_return("user://1634645541.save")

    subject.save_slot_handler.cur_slot = 0

    subject._on_save_pressed()

    assert_called(subject.popup, "set_default_text")
    assert_not_called(subject, "update_save_slot")

func test_when_on_save_pressed_is_called_cur_slot_is_not_zero():
    stub(subject, "create_save_slot").to_do_nothing()
    stub(subject, "update_save_slot").to_do_nothing()
    stub(subject.file_handler, "create_save_file_name").to_return("user://1634645541.save")

    subject.save_slot_handler.cur_slot = 1

    subject._on_save_pressed()

    assert_called(subject, "update_save_slot")
    assert_not_called(subject.popup, "set_default_text")

class TestOnDeletePressed:
    extends "res://addons/gut/test.gd"

    var subject = null

    func before_each():
        subject = partial_double("res://src/menus/save_system/save/save.gd").new()
        subject.slots = autofree(VBoxContainer.new())
        subject.save_slot_handler = autofree(preload("res://src/menus/save_system/components/save_slot_handler.gd").new(subject, subject.slots))
        subject.file_handler = double("res://src/menus/save_system/components/file_handler.gd").new()

        for _number in range(3):
            subject.slots.add_child(autofree(Button.new()))

        subject.setup_screen = autofree(load("res://src/menus/components/setup_screen.gd").new(subject))
        subject.setup_screen.buttons = {"delete" : autofree(Button.new())}

    func test_when_on_delete_pressed_is_called_current_slot_zero():
        subject.save_slot_handler.cur_slot = 0
        subject.setup_screen.buttons["delete"].disabled = false

        subject._on_delete_pressed()

        assert_called(subject.file_handler, "remove_save_file_by_index")
        assert_eq(subject.slots.get_child_count(), 2, "Remove Current Save Slot was called")
        assert_eq(subject.setup_screen.buttons["delete"].disabled, true, "Delete button disabled property updated to true")

    func test_when_on_delete_pressed_is_called_current_slot_not_zero():
        subject.save_slot_handler.cur_slot = 1
        subject.setup_screen.buttons["delete"].disabled = false
        subject._on_delete_pressed()
        assert_called(subject.file_handler, "remove_save_file_by_index")
        assert_eq(subject.slots.get_child_count(), 2, "Remove Current Save Slot was called")
        assert_eq(subject.setup_screen.buttons["delete"].disabled, false, "Delete button disabled property wasn't updated")

func test_when_on_popup_file_name_is_called():
    subject.data["save_slot"] = ""
    stub(subject, "update_data").to_do_nothing()
    subject.file_handler.save_files = ["user://TEST.save"]
    subject.popup.visible = true

    subject._on_popup_file_name("TEST")

    assert_eq(subject.slots.get_child(subject.slots.get_child_count() - 1).text, "TEST", "Create Save Slot was called")
    assert_called(subject, "update_data")
    assert_called(subject.file_handler, "open_save_file")
    assert_eq(subject.popup.visible, false, "Popup was hidden")
    assert_eq(subject.slots.get_child(1).pressed, true, "The button pressed property with index one was updated to true")

class TestOnVisibilityChanged:
    extends "res://addons/gut/test.gd"

    var subject = null
    
    func before_each():
        subject = partial_double("res://src/menus/save_system/save/save.gd").new()
    
    func test_when_on_visibility_changed_is_called_visible_true():
        subject.visible = true
        stub(subject, "refresh_screen").to_do_nothing()

        subject._on_visibility_changed()

        assert_called(subject, "refresh_screen")

    func test_when_on_visibility_changed_is_called_visible_false():
        subject.visible = false

        subject._on_visibility_changed()

        assert_not_called(subject, "refresh_screen")

func test_when_update_save_delete_buttons_is_called():
    subject.setup_screen.buttons["delete"].disabled = false
    subject.setup_screen.buttons["save"].disabled = false

    subject.update_save_delete_buttons(true)
    assert_eq(1,1)
    assert_eq(subject.setup_screen.buttons["delete"].disabled, true, "Delete button disabled property has been updated to true")
    assert_eq(subject.setup_screen.buttons["save"].disabled, true, "Save button disabled property has been updated to true")

func test_when_refresh_screen_is_called():
    for _number in range(3):
        subject.slots.add_child(autofree(Button.new()))

    stub(subject, "update_save_delete_buttons").to_do_nothing()

    subject.refresh_screen()

    assert_called(subject.file_handler, "refresh_save_files_list")
    assert_eq(subject.slots.get_child_count(), 1, "Refesh save slots was called")
    assert_called(subject, "update_save_delete_buttons", [true])

func test_when_switch_panel_is_called():
    watch_signals(subject)
    
    subject.switch_panel("pause")

    assert_signal_emitted_with_parameters(subject, "update_menu_controller", ["switch_panel", "pause"])
        