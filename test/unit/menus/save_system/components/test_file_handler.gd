extends "res://addons/gut/test.gd"

var subject = null
var test_file_name = null
var test_file = null

func before_each():
    subject = partial_double("res://src/menus/save_system/components/file_handler.gd").new()
    add_child_autofree(subject)

    test_file_name = "user://TEST.save"
    test_file = File.new()
    if test_file.open(test_file_name, File.WRITE) != OK:
        push_error("Failed to open TEST file")

func after_each():
    var dir := Directory.new()
    if dir.file_exists(test_file_name):
        if dir.remove(test_file_name) != OK:
            push_error("Failed to remove save file")

func test_when_list_files_in_save_directory_is_called():
    assert_eq(subject.list_files_in_save_directory().has("user://TEST.save"), true, "The Test file was within the return list")

func test_when_open_save_file_is_called_with_write_data():
    subject.save_files = []
    stub(subject, "open_file").to_return(test_file)

    assert_eq(subject.open_save_file("user://TEST.save", "TEST"), null, "Retern null becase data is being saved")
    assert_eq(subject.save_files.has("user://TEST.save"), true, "Save Files was updated with the new save file path")
    assert_called(subject, "open_file", ["user://TEST.save", File.WRITE])

func test_when_open_save_file_is_called_without_write_data():
    subject.open_save_file("user://TEST.save", "TEST")

    assert_eq(subject.open_save_file("user://TEST.save"), "TEST", "Returned data from save file")
    assert_called(subject, "open_file", ["user://TEST.save", File.READ])

func test_when_open_file_is_called():
    var file = subject.open_file("user://TEST.save", File.WRITE)

    assert_ne(file, null, "A new file was opened")

    file.close()

func test_when_remove_save_file_by_index_is_called():
    subject.save_files = ["user://TEST.save"]
    subject.remove_save_file_by_index(0)

    assert_eq(subject.list_files_in_save_directory().has("user://TEST.save"), false, "The Test file was removed")
    assert_eq(subject.save_files.size(), 0, "The path was removed from the save files array")

func test_when_create_save_file_name_is_called_save_file_not_exist():
    subject.temp_save_file = null
    assert_eq(subject.create_save_file_name(), str(OS.get_unix_time()), "The save file name was returned")
    assert_eq(subject.temp_save_file, subject.SAVES_PATH + str(OS.get_unix_time()) + subject.SAVE_ENDING, "The temp save file updated")

func test_when_create_save_file_name_is_called_save_file_does_exist():
    subject.save_files = [subject.SAVES_PATH + str(OS.get_unix_time()) + subject.SAVE_ENDING]
    assert_eq(subject.create_save_file_name(), null, "Null was returned")
    assert_eq(subject.temp_save_file, subject.SAVES_PATH + str(OS.get_unix_time()) + subject.SAVE_ENDING, "The temp save file updated")
    
func test_when_refresh_save_files_list_is_called():
    var test_save_files_list = ["user://1637083225.save", "user://1637083266.save"]
    stub(subject, "list_files_in_save_directory").to_return(test_save_files_list)

    subject.refresh_save_files_list()

    assert_called(subject, "list_files_in_save_directory")
    assert_eq(subject.save_files, test_save_files_list, "Save Files list was updated")

func test_when_load_save_file_by_index_is_called():
    subject.save_files = ["0", "1", "user://TEST.save"]
    stub(subject, "open_save_file").to_return({"save_slot" : "TEST"})
    var loaded_file = subject.load_save_file_by_index(2)
    assert_called(subject, "open_save_file", ["user://TEST.save", null])
    assert_eq(loaded_file["save_slot"], "TEST")
