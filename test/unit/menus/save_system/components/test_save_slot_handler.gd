extends "res://addons/gut/test.gd"

var subject = null
var parent = null
var slot = null

func before_each():
    parent = partial_double("res://src/menus/save_system/save/save.tscn").instance()
    add_child_autofree(parent)
    parent.file_handler = double("res://src/menus/save_system/components/file_handler.gd").new()

    slot = autofree(VBoxContainer.new())

    subject = preload("res://src/menus/save_system/components/save_slot_handler.gd").new(parent, slot)
    add_child_autofree(subject)

func test_when_create_save_slot_button_is_called():
    subject.create_save_slot_button("TEST")

    autofree(slot.get_child(0))

    assert_eq(slot.get_child_count(), 1)
    assert_eq(slot.get_child(0).text, "TEST")
    assert_eq(slot.get_child(0).toggle_mode, true)
    assert_connected(slot.get_child(0), parent, "pressed")

func test_when_create_slot_name_using_file_name_is_called():
    assert_eq(subject.create_slot_name_using_file_name("user://1634645541.save"), "2021/10/19 12:12:21", "Date and time returned for slot name")

func test_when_create_save_slot_is_called():
    slot.add_child(autofree(Button.new()))
    slot.add_child(autofree(Button.new()))

    subject.create_save_slot("TEST")

    assert_eq(slot.get_child(1).text, "TEST")

func test_when_move_current_save_slot_to_top_is_called():
    var test_button = autofree(Button.new())
    test_button.text = "TEST"
    subject.cur_slot = 2

    slot.add_child(autofree(Button.new()))
    slot.add_child(autofree(Button.new()))
    slot.add_child(test_button)

    subject.move_current_save_slot_to_top()

    assert_eq(slot.get_child(1).text, "TEST", "Text Button moved to first index")
    assert_eq(subject.cur_slot, 1, "Current Slot varable update to one")

func test_when_remove_current_save_slot_is_called():
    var test_buttons = [autofree(Button.new()), autofree(Button.new())]

    for index in test_buttons.size():
        test_buttons[index].text = "TEST" + str(index)
        slot.add_child(test_buttons[index])
        test_buttons[index].toggle_mode = true

    subject.cur_slot = 0

    watch_signals(test_buttons[1])

    subject.remove_current_save_slot()

    assert_eq(is_instance_valid(test_buttons[0]), false, "Button at current slot has been queued for deletion")
    assert_eq(subject.cur_slot, 0, "Current Slot varable wasn't updated")
    assert_eq(test_buttons[1].pressed, true)

func test_when_remove_all_save_slots_is_called():
    var test_buttons = [autofree(Button.new()), autofree(Button.new()), autofree(Button.new())]

    for index in test_buttons.size():
        test_buttons[index].text = "TEST" + str(index)
        slot.add_child(test_buttons[index])
        test_buttons[index].toggle_mode = true

    subject.cur_slot = 2

    subject.remove_all_save_slots()

    assert_eq(slot.get_child_count(), 1, "All Children removed except the first one")
    assert_eq(slot.get_child(0).text, "TEST0", "The correct button remains")
    assert_eq(subject.cur_slot, 0, "Current Slot updated to zero")   

func test_when_load_save_slots():
    stub(parent.file_handler, "open_save_file").to_return({"save_slot" : "TEST"}).when_passed("user://TEST.save", null)
    stub(parent.file_handler, "open_save_file").to_return({}).when_passed("user://1634645541.save", null)
    stub(parent.file_handler, "open_save_file").to_return(null).when_passed("FAIL", null)

    parent.file_handler.save_files = ["FAIL", "user://TEST.save", "user://1634645541.save"]

    subject.load_save_slots()

    assert_eq(slot.get_child_count(), 2, "A new save slot has been added to slots")
    assert_eq(slot.get_child(0).text, "TEST", "The correct slot name was used")
    assert_eq(slot.get_child(1).text, "2021/10/19 12:12:21", "Date and time used for slot name")

func test_when_refresh_save_slots_is_called():
    var test_buttons = [autofree(Button.new()), autofree(Button.new()), autofree(Button.new())]

    for index in test_buttons.size():
        test_buttons[index].text = "TEST" + str(index)
        slot.add_child(test_buttons[index])
        test_buttons[index].toggle_mode = true

    stub(parent.file_handler, "open_save_file").to_return({"save_slot" : "TEST_SAVE"}).when_passed("user://TEST.save", null)

    parent.file_handler.save_files = ["user://TEST.save"]

    subject.refresh_save_slots()

    assert_eq(slot.get_child_count(), 2, "Save Slots where removed and one was added")
    assert_eq(slot.get_child(0).text, "TEST0", "First Save Slot wasn't removed")
    assert_eq(slot.get_child(1).text, "TEST_SAVE", "SAVE Slot was added")