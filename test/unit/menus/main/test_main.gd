extends "res://addons/gut/test.gd"

var subject = null

func before_each():
    subject = load("res://src/menus/main/main.tscn").instance()
    add_child_autofree(subject)

func test_when_start_game_is_called():
    watch_signals(subject)

    subject.start_game()

    yield(yield_for(0.1),YIELD)

    autofree(get_tree().current_scene)

    assert_signal_emitted_with_parameters(subject, "update_menu_controller", ["continue_game", ""])
