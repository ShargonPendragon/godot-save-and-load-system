extends "res://addons/gut/test.gd"

var subject = null
var controller = null

func before_each():
    var dir := Directory.new()
    if dir.open("user://") == OK and dir.list_dir_begin(true) == OK:
        var file_name := dir.get_next()
        while not file_name == "":
            if not dir.current_is_dir() and file_name.ends_with(".save"):
                if dir.remove(file_name) != OK:
                    push_error("Failed to remove save file")
            file_name = dir.get_next()

    var test_file = File.new()
    if test_file.open("user://TEST.save", File.WRITE) != OK:
        push_error("Failed to open TEST file")
    test_file.store_var({"save_slot" : "TEST"})
    test_file.close()
        
    subject = autofree(preload('res://integration/menus.tscn').instance())
    add_child_autofree(subject)
    
    controller = subject.get_node("controller")
    controller.get_node("CenterContainer").get_node("load").free()

    var load_double = partial_double("res://src/menus/save_system/load/load.tscn").instance()
    controller.get_node("CenterContainer").add_child(load_double)
    controller.get_panels(controller)
    load_double.connect("update_menu_controller", controller, "update_menu_controller")

    controller.visible = true
    controller.switch_panel("load")

func after_each():
    subject.paused = false

    var dir := Directory.new()
    if dir.file_exists("user://TEST.save"):
        if dir.remove("user://TEST.save") != OK:
            push_error("Failed to remove save file")

func test_when_load_button_is_pressed_when_save_slot_is_selected():
    stub(controller.screen_panels["load"], "_on_load_pressed")
    subject.press_keys = ["ui_focus_prev", "ui_accept", "ui_focus_next", "ui_accept"]
    subject.paused = true
    print(controller.current_panel.get_name())
    yield(yield_for(1),YIELD)

    assert_eq(controller.current_panel.get_name(), "load")
    assert_called(controller.screen_panels["load"], "_on_load_pressed")

func test_when_delete_is_pressed():
    subject.press_keys = ["ui_focus_prev", "ui_accept", "ui_focus_next", "ui_focus_next", "ui_accept"]
    subject.paused = true
    yield(yield_for(1),YIELD)

    assert_eq(controller.current_panel.slots.get_children().size() , 1, "File slot has been removed")
    assert_eq(controller.current_panel.file_handler.save_files.size(), 0, "Generated save file removed from to save files list")

func test_when_back_is_pressed():
    subject.press_keys = ["ui_focus_next", "ui_focus_next", "ui_accept"]
    subject.paused = true
    watch_signals(controller.screen_panels["load"])

    yield(yield_for(1),YIELD)

    assert_signal_emitted(controller.screen_panels["load"], "update_menu_controller")
    assert_eq(controller.current_panel.get_name(), "pause")