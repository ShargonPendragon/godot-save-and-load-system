extends "res://addons/gut/test.gd"

var subject = null
var controller = null

func before_each():
    var dir := Directory.new()
    if dir.open("user://") == OK and dir.list_dir_begin(true) == OK:
        var file_name := dir.get_next()
        while not file_name == "":
            if not dir.current_is_dir() and file_name.ends_with(".save"):
                if dir.remove(file_name) != OK:
                    push_error("Failed to remove save file")
            file_name = dir.get_next()

    subject = autofree(preload('res://integration/menus.tscn').instance())
    add_child_autofree(subject)

    controller = subject.get_node("controller")
    controller.get_node("CenterContainer").get_node("save").free()

    var save_double = partial_double("res://src/menus/save_system/save/save.tscn").instance()
    controller.get_node("CenterContainer").add_child(save_double)
    controller.get_panels(controller)
    save_double.connect("update_menu_controller", controller, "update_menu_controller")

    controller.visible = true
    controller.switch_panel("save")

func after_each():
    controller.visible = false
    subject.paused = false

func test_when_save_button_is_pressed_when_new_slot_is_selected():
    subject.press_keys = ["ui_focus_prev", "ui_accept", "ui_focus_next", "ui_accept"]
    subject.paused = true
    controller.current_panel.popup.visible = false

    yield(yield_for(1),YIELD)

    assert_eq(controller.current_panel.popup.visible, true)

func test_when_save_popup_accept_is_pressed():
    subject.press_keys = ["ui_focus_prev", "ui_accept", "ui_focus_next", "ui_accept", "ui_focus_next", "ui_accept"]
    subject.paused = true
    controller.current_panel.popup.visible = false
    stub(controller.current_panel, "update_data")

    yield(yield_for(1),YIELD)

    var created_save_slot = controller.current_panel.slots.get_child(1)
    assert_eq(controller.current_panel.popup.visible, false, "Popup have been made invisiable")
    assert_eq(created_save_slot.pressed , true, "The first save slot have been pressed")
    assert_eq(controller.current_panel.file_handler.save_files.size(), 1, "Generated save file added to save files list")

    autofree(created_save_slot)

func test_when_save_popup_cancel_is_pressed():
    subject.press_keys = ["ui_focus_prev", "ui_accept", "ui_focus_next", "ui_accept", "ui_focus_next", "ui_focus_next", "ui_accept"]
    subject.paused = true
    controller.current_panel.popup.visible = false
    stub(controller.current_panel, "update_data")

    yield(yield_for(1),YIELD)

    assert_eq(controller.current_panel.popup.visible, false, "Popup have been made invisiable")
    assert_eq(controller.current_panel.slots.get_child(0).pressed , true, "The first save slot have been pressed")
    assert_eq(controller.current_panel.file_handler.save_files.size(), 0, "Generated save file added to save files list")

func test_when_delete_is_pressed():
    subject.press_keys = ["ui_focus_prev", "ui_accept", "ui_focus_next", "ui_accept", "ui_focus_next", "ui_accept", "ui_accept","ui_focus_next", "ui_focus_next", "ui_accept"]
    subject.paused = true
    controller.current_panel.popup.visible = false
    stub(controller.current_panel, "update_data")

    yield(yield_for(1),YIELD)

    assert_eq(controller.current_panel.slots.get_child(0).pressed , true, "The first save slot have been pressed")
    assert_eq(controller.current_panel.slots.get_children().size() , 1, "Save slot has been removed")
    assert_eq(controller.current_panel.file_handler.save_files.size(), 0, "Generated save file removed from to save files list")

func test_when_back_is_pressed():
    subject.press_keys = ["ui_focus_next", "ui_focus_next", "ui_accept"]
    subject.paused = true
    watch_signals(controller.screen_panels["save"])

    yield(yield_for(1),YIELD)

    assert_signal_emitted(controller.screen_panels["save"], "update_menu_controller")
    assert_eq(controller.current_panel.get_name(), "pause")