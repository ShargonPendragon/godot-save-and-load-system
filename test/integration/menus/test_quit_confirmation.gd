extends "res://addons/gut/test.gd"

var subject = null
var controller = null

func before_each():
    subject = preload('res://integration/menus.tscn').instance()
    add_child_autofree(subject)

    controller = subject.get_node("controller")

func test_when_no_button_is_pressed():
    subject.press_keys = ["ui_cancel", "ui_focus_next","ui_focus_next", "ui_focus_next", "ui_focus_next", "ui_accept", "ui_focus_next", "ui_accept"]
    subject.paused = false

    yield(yield_for(1), YIELD)

    assert_eq(controller.visible, true, "Menu Controler is visible")
    assert_eq(controller.current_panel.visible, true, "Current Panel is visible")
    assert_eq(controller.current_panel.name, "pause", "Current panel has been updated to null")
    assert_eq(subject.paused, true, "The game is paused when in the load menu")
