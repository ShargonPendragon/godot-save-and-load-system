extends "res://addons/gut/test.gd"

var subject = null
var controller = null

func before_each():
    subject = preload('res://integration/menus.tscn').instance()
    add_child_autofree(subject)
    controller = subject.get_node("controller")
    controller.get_node("CenterContainer").get_node("game_over").free()

    var double_game_over = partial_double("res://src/menus/game_over/game_over.tscn").instance()
    controller.get_node("CenterContainer").add_child(double_game_over)
    controller.get_panels(controller)
    double_game_over.connect("update_menu_controller", controller, "update_menu_controller")
    controller.switch_panel("game_over")

func test_when_try_again_button_is_pressed():
    stub(controller.screen_panels["game_over"], "reset_game")
    subject.press_keys = ["ui_accept"]
    subject.paused = false
   
    yield(yield_for(3), YIELD)

    assert_called(controller.screen_panels["game_over"], "reset_game")
