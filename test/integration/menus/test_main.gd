extends "res://addons/gut/test.gd"

var subject = null
var controller = null

func before_each():
    subject = preload('res://integration/menus.tscn').instance()
    add_child_autofree(subject)

    controller = subject.get_node("controller")
    controller.get_node("CenterContainer").get_node("main").free()

    var save_double = partial_double("res://src/menus/main/main.tscn").instance()
    controller.get_node("CenterContainer").add_child(save_double)
    controller.get_panels(controller)

    controller.visible = true
    controller.switch_panel("main")

func after_each():
    controller.switch_panel("")
    controller.visible = false
    subject.paused = false
    
func test_when_new_game_is_pressed():
    subject.press_keys = ["ui_accept"]
    subject.paused = true
    stub(controller.screen_panels["main"], "start_game")

    yield(yield_for(1), YIELD)

    assert_called(controller.screen_panels["main"], "start_game")
