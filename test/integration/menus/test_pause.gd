extends "res://addons/gut/test.gd"

var subject = null
var controller = null

func before_each():
    subject = preload('res://integration/menus.tscn').instance()
    add_child_autofree(subject)

    controller = subject.get_node("controller")

func after_each():
    controller.visible = false
    subject.paused = false
    
    
func test_when_pause_screen_is_displayed():
    subject.press_keys = ["ui_cancel"]
    subject.paused = false

    yield(yield_for(1), YIELD)

    assert_eq(controller.visible, true, "Menu Controler is visible")
    assert_eq(controller.current_panel.visible, true, "Current Panel is visible")
    assert_eq(controller.current_panel.name, "pause", "Pause is the current panel")
    assert_eq(subject.paused, true, "The game is paused when the pause screen is up")

func test_when_continue_button_is_pressed():
    subject.press_keys = ["ui_cancel", "ui_accept"]
    subject.paused = false

    yield(yield_for(1), YIELD)

    assert_eq(controller.visible, false, "Menu Controler is not visible")
    assert_eq(controller.current_panel, null, "Current panel has been updated to null")
    assert_eq(subject.paused, false, "The game is not paused when the pause menu is not up")

func test_when_save_button_is_pressed():
    subject.press_keys = ["ui_cancel", "ui_focus_next", "ui_accept"]
    subject.paused = false

    yield(yield_for(1), YIELD)

    assert_eq(controller.visible, true, "Menu Controler is not visible")
    assert_eq(controller.current_panel.visible, true, "Current Panel is visible")
    assert_eq(controller.current_panel.name, "save", "Current panel has been updated to null")
    assert_eq(subject.paused, true, "The game is paused when in the save menu")

func test_when_load_button_is_pressed():
    subject.press_keys = ["ui_cancel", "ui_focus_next", "ui_focus_next", "ui_accept"]
    subject.paused = false

    yield(yield_for(1), YIELD)

    assert_eq(controller.visible, true, "Menu Controler is not visible")
    assert_eq(controller.current_panel.visible, true, "Current Panel is visible")
    assert_eq(controller.current_panel.name, "load", "Current panel has been updated to null")
    assert_eq(subject.paused, true, "The game is paused when in the load menu")
    
func test_when_quit_button_is_pressed():
    subject.press_keys = ["ui_cancel", "ui_focus_next", "ui_focus_next", "ui_focus_next", "ui_focus_next", "ui_accept"]
    subject.paused = false

    yield(yield_for(1), YIELD)

    assert_eq(controller.visible, true, "Menu Controler is not visible")
    assert_eq(controller.current_panel.visible, true, "Current Panel is visible")
    assert_eq(controller.current_panel.name, "quit_confirmation", "Current panel has been updated to null")
    assert_eq(subject.paused, true, "The game is paused when in the load menu")
    