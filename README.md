# GODOT SAVE AND LOAD SYSTEM
This template provides a basic menu with a functioning save and load system.

## Getting Started
1. Clone the Repo
2. Open in Godot

### Using In Existing Projects
Copy the `assets` and `src` folder into your project(make sure you copy the LICENSE.txt as well)


## Usage

### Main Menu
Upon running this template in Godot. You will be greeted with a Main Menu Screen.
`Exit Game`, of course, exits the game.

`New Game` will take you to a blank screen which is where your game would go. Hit the `esc` key to bring up the pause menu. 

### Pause Menu
Each of the buttons do the following:
* `Continue` Returns to the current game
* `Save` Brings up the Save Screen
* `Load` Brings up the Load Screen (looks the exact same as the save screen)
* `Title Menu` Returns to `Main Menu` Screen
* `Exit Game` Exits the game.

### Save Menu
* Clicking on any slot will activate the `Save` Button.
* If you clicked on `New Slot` then clicked the `Save` button you will prompted to enter your save file name.
* If you clicked on a save file that already exists then clicked the `Save` button it will save data to that slot.
* If you clicked on a save file that already exists the `Delete` Button will also activate allowing you to delete the save file.
* The `Back` button returns to the previous menu.

### Load Menu
Looks exactly like the `Save Menu` sans the `New Slot` and subbing the `Load` for the `Save` Button. Clicking any slot then clicking `Load` will load the file.

## Examples

### Main Menu:

![Main Menu](screenshots/Main_Menu.png)

### Pause Menu:

![Pause Menu](screenshots/Pause_Menu.png)

### Save Menu:

![Save Menu](screenshots/Save_Menu.png)

## Running Unit/Integration Tests

1. Install [GUT Command Line](https://github.com/bitwes/Gut/wiki/Command-Line)
2. Set the `gut` alias detailed in the instructions above
3. open you terminal
4. run `gut`

NOTE: Some tests are failing when using WSL. These tests are confirmed to work on both Linux and Mac.

## Contributing

1. Fork The Repo
2. Make Changes
3. Add Unit and Integration Tests
4. Submit PR

NOTE: PRs will not be reviewed until all tests are passing.

## References

The save system is a heavily modified version of the [Godot Infinite Save and Load System](https://github.com/ShatReal/infinite-save-system) 

Icon provided by Gobusto at https://opengameart.org/content/floppy-disk
